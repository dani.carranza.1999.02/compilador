from lex import *
from emit import *
from parse import *
import sys

def main():
    print("Compilador Locoshon")

    if len(sys.argv) != 2:
        sys.exit("Error: Compiler necesita un archivo origen como argumento.")
    with open(sys.argv[1], 'r') as inputFile:
        source = inputFile.read()

    # Initialize the lexer, emitter, and parser.
    lexer = Lexer(source)
    emitter = Emitter("resultao.c")
    parser = Parser(lexer, emitter)

    parser.program() # Start the parser.
    emitter.writeFile() # Write the output to file.
    print("Compilado completao.")

main()
